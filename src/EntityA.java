import java.awt.*;

public interface EntityA {
    //EntityClass for PlayerObjects
    public void tick();
    public void render(Graphics g);
    public Rectangle getBounds();

    public double getX();
    public double getY();


}
