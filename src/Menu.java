import java.awt.*;
import java.awt.image.BufferedImage;


// Backgroundimage was taken from http://pixelartmaker.com/art/9622fa83642c4dc
// Krater was taken from https://www.pngfind.com/mpng/iwiihb_06-feb-2009-circle-fire-effect-png-transparent/
// Smoke was taken from https://www.pngfind.com/download/himoT_clipart-gun-free-collection-download-and-share-/

public class Menu {

    public Rectangle playButton = new Rectangle(GameBoard.WIDTH/ 2 + 120 ,150,100,50);
    public Rectangle quitButton = new Rectangle(GameBoard.WIDTH/ 2 + 120,250,100,50);
    //define the menu
    public void render(Graphics g){
        Graphics2D g2d = (Graphics2D) g;

        Font fnt0 = new Font("arial",Font.BOLD,50);
        g.setFont(fnt0);
        g.setColor(Color.black);
        g.drawString(" Beach Defender", GameBoard.WIDTH/ 2 - 40, 100);

        Font fnt1 = new Font("arial",Font.BOLD,30);
        g.setFont(fnt1);
        g.drawString("Play", playButton.x +20, playButton.y+35);
        g2d.draw(playButton);
        g.drawString("Quit", quitButton.x +20, quitButton.y+35);
        g2d.draw(quitButton);

    }
    public void renderGameOver(Graphics g){
        g.setColor(Color.gray);
        g.fillRect(5,5,200,25);
        g.setColor(Color.green);
        g.fillRect(5,5,GameBoard.HEALTH,25);
        g.setColor(Color.white);
        g.drawRect(5,5,200,25);
        // Draw Score
        g.setColor(Color.black);
        g.drawRect(450,5, 150,25);
        Font fnt0 = new Font("arial",Font.BOLD,20);
        g.setFont(fnt0);
        g.setColor(Color.black);
        g.drawString("Score: " + GameBoard.SCORE,465,25);
        //Draw Game Over
        Font fnt1 = new Font("arial",Font.BOLD,60);
        g.setFont(fnt0);
        g.setColor(Color.red);
        g.drawString("You Died! ",270,220);

    }

}
