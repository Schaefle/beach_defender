import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

// The Image for the enemies (Meteors) was taken from https://www.pngegg.com/en/png-zvdhq and modified
// via https://editor.method.ac/

public class Enemy extends GameObject implements EntityB{

    Random r = new Random();
    private int speed = r.nextInt(2)+1;

    private Textures tex;
    private GameBoard game;
    private Controller c;

    public Enemy(double x,double y, Textures tex,Controller c,GameBoard game){
        super(x,y);
        this.tex = tex;
        this.c = c;
        this.game = game;
    }
    public void tick(){
        //set Speed of enemies to random
        y+=speed;
        //set spawn of enemies random on the xAxes and reposition them if they hit the ground
        if(y > GameBoard.HEIGHT*GameBoard.SCALE){
            c.removeEntity(this);
            GameBoard.HEALTH-=20;
            game.setEnemy_killed(game.getEnemy_killed()+1);

        }
        //Collision friendly entity and enemy entity, remove both from linkedlist
        for(int i = 0; i<game.ea.size() ;i++){
            EntityA tempEnt = game.ea.get(i);
            if(Physics.Collision(this,tempEnt)){
                c.removeEntity(tempEnt);
                c.removeEntity(this);
                game.setEnemy_killed(game.getEnemy_killed()+1);
                game.setSCORE(game.getSCORE()+10);
            }
        }

    }
    public void render(Graphics g){
        g.drawImage(tex.enemy, (int) x,(int) y,null);
    }

    public Rectangle getBounds(){
        return new Rectangle((int)x,(int)y,32,32);
    }

    public double getX(){
        return x;
    }

    public void setX(double x){
        this.x = x;
    }

    public double getY(){
        return y;
    }
    public void setY(double y){
        this.y = y;
    }
}