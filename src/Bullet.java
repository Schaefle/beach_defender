import java.awt.*;
import java.awt.image.BufferedImage;

/*Bullet was drawn by myself via https://editor.method.ac/
*/

public class Bullet extends GameObject implements EntityA{
    private Textures tex;
    private GameBoard game;

    public Bullet(double x, double y, Textures tex, GameBoard game){
        super(x,y);
        this.tex = tex;
        this.game = game;
    }

    public  void tick(){
        // Defines the Speed of the Bullet
        y-=10;
    }
    //draw image from Bullet as saved in the spriteSheet
    public void render(Graphics g){
        g.drawImage(tex.bullet, (int) x,(int) y,null);
    }
    public Rectangle getBounds(){
        return new Rectangle((int)x+8,(int)y,16,32);
    }

    public double getX() {
        return x;
    }

    public double getY(){
        return y;
    }

}
