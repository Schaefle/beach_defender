import java.awt.*;
import java.awt.image.BufferedImage;
import java.nio.Buffer;

// Player image was taken from https://www.vhv.rs/viewpic/iwRxJJb_http-i-stack-imgur-com-q7sei-game-sprites/ and modified
// via https://editor.method.ac/
public class Player extends GameObject implements EntityA {

    //used in tick to smoothen the movement
    private double velX = 0;
    private Textures tex;
    private GameBoard game;
    private Controller c;

    public Player(double x, double y, Textures tex, GameBoard game,Controller c){
        super(x,y);
        this.tex = tex;
        this.game = game;
        this.c = c;
    }

    public void tick(){
        x+=velX;
        //Check the bounds of the GameBoard
        if(x <=0)
            x=0;
        if(x>=600)
            x=600;
        //Collision Detetection with enemy, only remove enemy entity from list, decrease health and check Gameover
        for(int i = 0; i < game.eb.size();i++){
            EntityB tempEnt = game.eb.get(i);
            if (Physics.Collision(this, tempEnt)) {
                c.removeEntity(tempEnt);
                GameBoard.HEALTH-=20;
                game.setEnemy_killed(game.getEnemy_killed()+1);
            }
            if(GameBoard.HEALTH <=0){
                GameBoard.state = GameBoard.STATE.OVER;
            }
        }
    }

    public void render(Graphics g){
        g.drawImage(tex.player, (int) x,(int) y,null);
    }


    public Rectangle getBounds(){
        return new Rectangle((int)x,(int)y,32,32);
    }
    public double getX(){
        return x;
    }

    public void setX(double x){
        this.x = x;
    }

    public void setVelX(double velX){
        this.velX =velX;
    }

    public double getY(){
        return y;
    }
    public void setY(double y){
        this.y = y;
    }
}
