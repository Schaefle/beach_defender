import java.awt.image.BufferedImage;

public class Textures {

    public BufferedImage player,bullet, enemy;
    private SpriteSheet ss = null;

    public Textures(GameBoard game){
        ss = new SpriteSheet(game.getSpriteSheet());

        getTextures();
    }
    // defines with image is taken from the sheet
    private void getTextures(){
        player = ss.grabImage(1,1,32,32);
        bullet = ss.grabImage(2,1,32,32);
        enemy = ss.grabImage(3,1,32,32);
    }
}
