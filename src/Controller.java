import java.awt.*;
import java.util.LinkedList;
import java.util.Random;


// handles the Entitylists of friendly Units and Enemies
public class Controller {

    private LinkedList<EntityA> ea = new LinkedList<EntityA>();
    private LinkedList<EntityB> eb = new LinkedList<EntityB>();
    EntityA enta;
    EntityB entb;

    private Textures tex;
    Random r = new Random();
    private GameBoard game;

    public Controller(Textures tex, GameBoard game){
        this.tex = tex;
        this.game = game;
    }
    //add enemy entity to list
    public void createEnemy(int enemy_count){
        for (int i =0;i<enemy_count;i++){
            addEntity(new Enemy(r.nextInt(500),0,tex, this, game));
        }
    }
    //movement of the entiteis
    public void tick(){
        //A CLASS friendly Objects
        for(int i= 0; i<ea.size();i++){
            enta = ea.get(i);
            enta.tick();
        }
        //B CLASS enemies
        for(int i= 0; i<eb.size();i++){
            entb = eb.get(i);
            entb.tick();
        }
    }
    public void render(Graphics g){
        //A CLASS friendly Objects
        for(int i= 0; i<ea.size();i++){
            enta = ea.get(i);
            enta.render(g);
        }
        //B CLASS enemies
        for(int i= 0; i<eb.size();i++){
            entb = eb.get(i);
            entb.render(g);
        }
    }

    public void addEntity(EntityA block){
        ea.add(block);
    }
    public void removeEntity(EntityA block){
        ea.remove(block);
    }
    public void addEntity(EntityB block){
        eb.add(block);
    }
    public void removeEntity(EntityB block){
        eb.remove(block);
    }
    public LinkedList<EntityA> getEntityA(){return ea;}
    public LinkedList<EntityB> getEntityB(){return eb;}
}
