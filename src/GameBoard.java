import java.awt.*;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.*;
import java.nio.Buffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

import javax.imageio.*;
import java.awt.image.*;
import java.io.*;


public class GameBoard  extends Canvas implements Runnable
{
    //Declare size and Title of the board
    public static final long serialVersionUID = 1L;
    public static final int WIDTH = 320;
    public static final int HEIGHT = WIDTH/12*9;
    public static final int SCALE = 2;
    public final String TITLE = "Beach Defender";

    private boolean running = false;
    private Thread thread;

    // initialize Buffer
    private BufferedImage image = new BufferedImage(WIDTH,HEIGHT,BufferedImage.TYPE_INT_RGB);
    private BufferedImage spriteSheet = null;
    private BufferedImage background = null;
    public static BufferedImage gameOver = null;

    private boolean is_shooting = false;

    private int enemy_count = 1;
    private int enemy_killed = 0;

    private Player p;
    private Controller c;
    private Textures tex;
    private Menu menu;

    public LinkedList<EntityA> ea;
    public LinkedList<EntityB> eb;

    public static int HEALTH = 100 * 2;
    public static int SCORE = 0;

    public static enum STATE{
        MENU,
        GAME,
        OVER
    };

    public static STATE state= STATE.MENU;
    //initializes all neccessary images from Spritesheet as well as all other needed classes like Mouselistenr Controller etc.
    public void init(){
        requestFocus();
        BufferedImageLoader loader = new BufferedImageLoader();
        try {
            spriteSheet = loader.loadImage("/SpriteSheet.png");
            background = loader.loadImage("/Background.png");
            gameOver =loader.loadImage("/GameOver.png");
        }catch (IOException e){
            e.printStackTrace();
        }
        addKeyListener(new KeyInput(this));
        tex = new Textures(this);
        //set coordinats of player at thee middle of x and a little above
        //init Controller and Menu
        c = new Controller(tex,this);
        p = new Player(288, 400,tex, this,c);
        menu = new Menu();

        ea = c.getEntityA();
        eb = c.getEntityB();

        this.addMouseListener(new MouseInput());

        c.createEnemy(enemy_count);
    }

    private synchronized void start(){
        if(running){
            return;
        }
        running = true;
        thread = new Thread(this);
        thread.start();
    }
    private synchronized void stop(){
        if(!running){
            return;
        }
        running = false;
        try{
            thread.join();
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        System.exit(1);
    }

    @Override

    public void run() {

        init();
        //set Framerate
        long lastTime = System.nanoTime();
        final double amountOfTicks = 60.0;
        double ns = 1000000000/ amountOfTicks;
        double delta = 0;
        int update = 0;
        int frames= 0;
        long timer = System.currentTimeMillis();

        //Game loop + frames stabilizer with 60 ticks per seconds
        while(running){
            long now = System.nanoTime();
            delta += (now - lastTime )/ns;
            lastTime = now;
            if(delta>=1){
                tick();
                update++;
                delta--;
            }
            render();
            frames++;
        }
        stop();
    }

    private void tick(){
        if(state == STATE.GAME){
            p.tick();
            c.tick();
        }
        if(enemy_killed >= enemy_count){
            enemy_count += 2;
            enemy_killed = 0;
            c.createEnemy(enemy_count);
        }

    }
    // handles the buffering behind the scene
    private void render(){

        BufferStrategy bs = this.getBufferStrategy();
        //set bufferstrategy once with tripplebuffer in case the pc is fast enough
        if(bs==null){
            createBufferStrategy(3);
            return;
        }
        //Draw the buffers
        Graphics g = bs.getDrawGraphics();
        ////////////////// draw stuff
        g.drawImage(background,0,0,null);
        //draw the actual Game
        if(state == STATE.GAME){
            p.render(g);
            c.render(g);
            // Draw healthbar
            g.setColor(Color.gray);
            g.fillRect(5,5,200,25);
            g.setColor(Color.green);
            g.fillRect(5,5,HEALTH,25);
            g.setColor(Color.white);
            g.drawRect(5,5,200,25);
            // Draw Score
            g.setColor(Color.black);
            g.drawRect(450,5, 150,25);
            Font fnt0 = new Font("arial",Font.BOLD,20);
            g.setFont(fnt0);
            g.setColor(Color.black);
            g.drawString("Score: " + SCORE,465,25);
        //draw the starting Menu
        }else if(state == STATE.MENU){
            menu.render(g);
        //draw GameOver
        }else if(state == STATE.OVER){
            g.drawImage(gameOver,0,0,null);
            menu.renderGameOver(g);
        }
        //////////////////
        g.dispose();
        bs.show();
    }

    public BufferedImage getSpriteSheet(){
        return spriteSheet;
    }

    public void keyPressed(KeyEvent e){
        int key = e.getKeyCode();
        //checks input of arrow keys to move player but only in GameState not in Menu or Gameover
        if(state == STATE.GAME) {
            if (key == KeyEvent.VK_RIGHT) {
                p.setVelX(7.0);
            } else if (key == KeyEvent.VK_LEFT) {
                p.setVelX(-7.0);
            } else if (key == KeyEvent.VK_SPACE && !is_shooting) {
                is_shooting = true;
                c.addEntity(new Bullet(p.getX(), p.getY(), tex, this));
            }
        }
    }
    public void keyReleased(KeyEvent e){
        int key = e.getKeyCode();
        if(key == KeyEvent.VK_RIGHT){
            p.setVelX(0);
        }else if (key == KeyEvent.VK_LEFT){
            p.setVelX(0);
        }else if (key== KeyEvent.VK_SPACE){
            is_shooting = false;
        }
    }

    public int getEnemy_count() {
        return enemy_count;
    }

    public int getEnemy_killed() {
        return enemy_killed;
    }

    public void setEnemy_count(int enemy_count) {
        this.enemy_count = enemy_count;
    }

    public void setEnemy_killed(int enemy_killed) {
        this.enemy_killed = enemy_killed;
    }

    public static int getSCORE() {
        return SCORE;
    }

    public static void setSCORE(int SCORE) {
        GameBoard.SCORE = SCORE;
    }

    public static void main(String args[]){

        GameBoard game = new GameBoard();
        game.setPreferredSize(new Dimension(WIDTH*SCALE,HEIGHT*SCALE));
        game.setMaximumSize(new Dimension(WIDTH*SCALE,HEIGHT*SCALE));
        game.setMinimumSize(new Dimension(WIDTH*SCALE,HEIGHT*SCALE));

        //Create and configure frame
        JFrame frame =new JFrame(game.TITLE);
        frame.add(game);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        //start game loop
        game.start();
    }
}//end of class
