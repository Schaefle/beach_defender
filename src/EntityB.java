import java.awt.*;

public interface EntityB {
    //EntityClass for EnemyObjects
    public void tick();
    public void render(Graphics g);
    public Rectangle getBounds();

    public double getX();
    public double getY();


}
