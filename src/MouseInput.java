import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MouseInput implements MouseListener {

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        int mx = e.getX();
        int my = e.getY();

        //Playbutton coordinates from MenuClass
        if(mx >= GameBoard.WIDTH/ 2 + 120 && mx <= GameBoard.WIDTH/ 2 + 220){
            if(my >=150 && my <= 200){
                //pressed Playbutton
                GameBoard.state = GameBoard.STATE.GAME;
            }
        }
        //Quit coordinates from MenuClass
        if(mx >= GameBoard.WIDTH/ 2 + 120 && mx <= GameBoard.WIDTH/ 2 + 220){
            if(my >=250 && my <= 300){
                //pressed Quit
               System.exit(1);
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
